<?php 
include(GetLangFileName(dirname(__FILE__)."/", "/payment.php"));

$server         = CSalePaySystemAction::GetParamValue("SERVER");
$seller_name    = CSalePaySystemAction::GetParamValue("SELLER_NAME");
$shop_id        = CSalePaySystemAction::GetParamValue("SHOP_ID");
$secret_code    = CSalePaySystemAction::GetParamValue("SECRET_CODE");
$button_text    = CSalePaySystemAction::GetParamValue("BUTTON_TEXT");
$email_on_error = CSalePaySystemAction::GetParamValue("EMAIL_ON_ERROR");
$amount         = number_format(
                    CSalePaySystemAction::GetParamValue("AMOUNT"),
                    2, ".", ""
                );
$order_id =  $GLOBALS["SALE_INPUT_PARAMS"]["ORDER"]["ID"];
$order_details = '';

/* Getting order details */
$items = CSaleBasket::GetList( null, array("ORDER_ID" => $order_id)); 
while ($item = $items->Fetch()) { 
    $order_details .= $item['NAME'] . ' x ' . $item['QUANTITY'] . ', '; 
}
$order_details = trim($order_details, ', ');

$output  = '<form action="' . $server . '" method="post">';
$output .= '<input type="hidden" name="order_id" '
    .'value="'.$order_id.'">';
$output .= '<input type="hidden" name="order_total" '
    .'value="'.$amount.'">';
$output .= '<input type="hidden" name="order_details" '
    .'value="'.$order_details.'">';
$output .= '<input type="hidden" name="seller_name" '
    .'value="'.$seller_name.'">';
$output .= '<input type="hidden" name="shop_id" '
    .'value="'.$shop_id.'">';
$output .= '<input type="submit" name="submit" '
    .'value="'.$button_text.'">';

echo $output;

?>
